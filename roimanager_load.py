from __future__ import with_statement
from ij import IJ, Menus
from ij.gui import PolygonRoi
from ij.plugin.frame import RoiManager
from ij.io import OpenDialog
import json
import os


default_conf = {
    "last_folder": None,
    "file_extension": "rmtd",
    "debug": True
}

# Some Java config, to tell RoiManager to create polygons from given coords.
POLYGON = PolygonRoi.POLYGON
CONFFNAME = os.path.join(Menus.getPlugInsPath(), "roimanager_dlt_conf.json")
DEBUG = False


def set_polygon(name, x, y, rm):
    """Create a PolygonRoi from given coordinate lists, and add it to the given
     RoiManager instance.
    
    Parameters
    ----------
    name : str
        Name the polygon will be given in the RoiManager.
    x : list
        Polygon x-coordinates.
    y : list
        See x.
    rm : RoiManager
    """
    n = rm.getCount()
    if DEBUG: IJ.log("length x, y: %s, %s" % (len(x), len(y)))
    if DEBUG: IJ.log("x: " + str(x))
    if DEBUG: IJ.log("y: " + str(y))
    rm.addRoi(PolygonRoi(x, y, len(x), POLYGON))
    rm.rename(n, name)


def get_filename(fldr, ext=None):
    if fldr is None:
        fldr = IJ.getDirectory("file")
    fpath = IJ.getImage().getTitle()
    fpath = "%s.%s" % (fpath[:fpath.rfind('.')], ext)
    if DEBUG: IJ.log("file name: '%s'" % fpath)
    diag = OpenDialog("Choose a file to load rois from.", fldr, fpath)
    return diag.getPath(), diag.getDirectory()


def get_config(fname):
    if os.path.exists(fname):
        with open(fname, mode='r') as fp:
            conf = json.load(fp)
    else:
        IJ.log("Could not find config file '%s'. Using default config."
               % fname)
        conf = default_conf.copy()
    if conf['debug']:
        IJ.log("CONFFNAME: " + CONFFNAME)
    return conf


if __name__ == "__main__":
    conf = get_config(CONFFNAME)
    DEBUG = conf['debug']
    rm = RoiManager.getInstance()
    if rm is None:
        rm = RoiManager()
    fpath, folder = get_filename(fldr=conf['last_folder'],
                                 ext=conf['file_extension'])
    if DEBUG:
        IJ.log("fpath: " + fpath)
    with open(fpath, mode='r') as fp:
        content = json.load(fp)
    npols = content['number of items']
    if DEBUG:
        IJ.log("npols: %d" % npols)
    for pol in content['polygons']:
        xy = pol['xy']
        set_polygon(pol['name'], xy[0], xy[1], rm)
