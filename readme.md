# `coordspicker`

Plugin for Fiji/ImageJ that allows to dump coordinates fromof several polygons
stored in ImageJ's `RoiManager` into a JSON file. Alternatively coordinates may
also be read from a JSON file and loaded into `RoiManager`. The `coordspicker`
package provides `load_rm_json()` function that de-serializes the JSON-formatted
coordinates to 2D numpy arrays.

## Installation

Get this repo and change with your shell into the fresh `coordspicker` folder:
```shell
git clone https://gitlab.com/isonder/coordspicker.git
cd coordspicker
```

### Fiji Plugin
Copy the plugin files `roimanager_dump.py`, `roimanager_load.py` and their
config file `roimanager_dlt_conf.json` to ImageJ's plugin folder.
These files will run in Fiji's builtin Jython engine. E.g.
```shell
cp roimanager_*.* path/to/Fiji.app/plugins
```

### `coordspicker` Package
Probably it is good advice to run this in a virtual environment. Then:
```shell
pip install -e .
```
