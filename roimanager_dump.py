from __future__ import with_statement
from ij import IJ, Menus
from ij.plugin.frame import RoiManager
from ij.io import OpenDialog
import json
import os

DEBUG = False
CONFFNAME = os.path.join(Menus.getPlugInsPath(), "roimanager_dlt_conf.json")
if DEBUG: IJ.log("CONFFNAME: " + CONFFNAME)


default_conf = {
    "last_folder": None,
    "file_extension": "rmtd",
    "debug": True
}


def get_filename(fldr=None, ext=None):
    if fldr is None:
        fldr = IJ.getDirectory("file")
    fpath = IJ.getImage().getTitle()
    fpath = "%s.%s" % (fpath[:fpath.rfind('.')], conf['file_extension'])
    if DEBUG:
        IJ.log("file name: '%s'" % fpath)
    diag = OpenDialog("Choose a file name to save rois to.", fldr, fpath)
    return diag.getPath(), diag.getDirectory()


def get_config(fname):
    if os.path.exists(fname):
        with open(fname, mode='r') as fp:
            conf = json.load(fp)
    else:
        IJ.log("Could not find config file '%s'. Using default config."
               % fname)
        conf = default_conf.copy()
    if conf['debug']:
        IJ.log("CONFFNAME: " + CONFFNAME)
    return conf


if __name__ == "__main__":
    conf = get_config(CONFFNAME)
    rm = RoiManager.getInstance()
    if rm is None:
        rm = RoiManager()
    rd = {
        "title": "roiManager text dump",
        "version": "0.1",
        "number of items": rm.count,
        "polygons": []
    }
    for i in range(rm.count):
        roi = rm.getRoi(i)
        poly = roi.getPolygon()
        x = list(poly.xpoints)
        y = list(poly.ypoints)
        rd["polygons"].append({
            "name": roi.getName(),
            "type": "polygon",
            "length": len(x),
            "xy": [x, y]
        })
    if DEBUG:
        IJ.log(json.dumps(rd))
    fpath, folder = get_filename(fldr=conf['last_folder'],
                                 ext=conf['file_extension'])
    with open(fpath, mode='wb') as fp:
        json.dump(rd, fp)
        conf['last_folder'] = folder
    if DEBUG:
        IJ.log("conf: " + str(conf))
    with open(CONFFNAME, 'w') as fp:
        json.dump(conf, fp, indent=2)
