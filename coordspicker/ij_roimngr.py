import numpy as np
from pathlib import Path
import json
from typing import Union


def load_rmjson(fname) -> dict:
    """Load ImageJ RoiManager style json data and return as dict.

    Parameters
    ----------
    fname : pathlib.Path or str
    """
    fname = Path(fname)
    with fname.open(mode='r') as fh:
        content = json.load(fh)
    if "polygons" in content.keys():
        for pol in content['polygons']:
            pol['xy'] = np.asarray(pol['xy']).T
    return content


def save_rmjson(content: dict, fname: Union[Path, str]):
    """Save polygon data from dictionary to ImageJ RoiManager json data.

    Parameters
    ----------
    content : dict
        Polygons and metadata.
    fname : pathlib.Path or str
        Target file.
    """
    fname = Path(fname)
    content = content.copy()
    if "polygons" in content.keys():
        for i, pol in enumerate(content['polygons']):
            xy = pol['xy']
            if isinstance(xy, np.ndarray):
                content['polygons'][i]['xy'] = [   # Necessary, since json does
                    [int(el) for el in xy[:, 0]],  # not handle numpy data types.
                    [int(el) for el in xy[:, 1]]
                ]
    with fname.open(mode='w') as fh:
        json.dump(content, fh)
