import unittest
import numpy as np
from coordspicker import load_rmjson, save_rmjson


class CoordspickerTestCase(unittest.TestCase):

    def setUp(self) -> None:
        self.load_content: dict = load_rmjson('../testdata/testpols.json')
        self.correct_content: dict = {
            "title": "roiManager text dump",
            "version": "0.1",
            "number of items": 4,
            "polygons": [{
                "xy": np.array([
                    [1171, 1162, 1147, 1139, 1127, 1123, 1118, 1110, 1102, 1099,
                     1101, 1116, 1125, 1137, 1146, 1158, 1169, 1184, 1204, 1225,
                     1249, 1281, 1308, 1337, 1361, 1390, 1420, 1447, 1477, 1496,
                     1508, 1514, 1524, 1530, 1535, 1544, 1544, 1537, 1527, 1521,
                     1507, 1498, 1483, 1469, 1453, 1442, 1427, 1186],
                    [-48, -33, -13, 5, 28, 54, 72, 99, 118, 138, 159, 181, 208,
                     230, 250, 265, 280, 294, 313, 327, 336, 340, 341, 339, 334,
                     330, 320, 302, 280, 251, 224, 198, 164, 143, 128, 109, 90,
                     79, 62, 38, 15, -8, -28, -45, -60, -75, -80, -66]]).T,
                "length": 48,
                "type": "polygon",
                "name": "ref_635"}, {
                "xy": np.array([
                    [1064, 1055, 1040, 1032, 1020, 1016, 1011, 1003, 995, 992,
                     994, 1009, 1018, 1030, 1039, 1051, 1062, 1077, 1097, 1118,
                     1142, 1174, 1201, 1230, 1254, 1283, 1313, 1340, 1370, 1389,
                     1401, 1407, 1417, 1423, 1428, 1437, 1437, 1430, 1420, 1414,
                     1400, 1391, 1376, 1362, 1346, 1335, 1320, 1079],
                    [2, 17, 37, 55, 78, 104, 122, 149, 168, 188, 209, 231, 258,
                     280, 300, 315, 330, 344, 363, 377, 386, 390, 391, 389, 384,
                     380, 370, 352, 330, 301, 274, 248, 214, 193, 178, 159, 140,
                     129, 112, 88, 65, 42, 22, 5, -10, -25, -30, -16]]).T,
                "length": 48,
                "type": "polygon",
                "name": "ref_636"}, {
                "xy": np.array([
                    [1072, 1063, 1048, 1040, 1028, 1024, 1019, 1011, 1003, 1000,
                     1002, 1017, 1026, 1038, 1047, 1059, 1070, 1085, 1105, 1126,
                     1150, 1182, 1209, 1238, 1262, 1291, 1321, 1348, 1378, 1397,
                     1409, 1415, 1425, 1431, 1436, 1445, 1445, 1438, 1428, 1422,
                     1408, 1399, 1384, 1370, 1354, 1343, 1328, 1087],
                    [41, 56, 76, 94, 117, 143, 161, 188, 207, 227, 248, 270, 297,
                     319, 339, 354, 369, 383, 402, 416, 425, 429, 430, 428, 423,
                     419, 409, 391, 369, 340, 313, 287, 253, 232, 217, 198, 179,
                     168, 151, 127, 104, 81, 61, 44, 29, 14, 9, 23]]).T,
                "length": 48,
                "type": "polygon",
                "name": "ref_637"}, {
                "xy": np.array([
                    [886, 870, 861, 848, 832, 828, 831, 822, 807, 804, 806, 821,
                     830, 842, 851, 863, 874, 889, 909, 930, 954, 986, 1013,
                     1042, 1066, 1095, 1125, 1152, 1182, 1201, 1213, 1219, 1229,
                     1235, 1231, 1233, 1210, 1196, 1184, 1176, 1158, 1143, 1136,
                     1125, 1113, 1119, 1118, 1109, 1097, 1078, 1067, 1055, 1051,
                     1037, 1010, 976, 954, 943, 918, 904, 907, 915, 925, 909],
                    [516, 514, 524, 533, 554, 580, 602, 628, 644, 664, 685, 707,
                     734, 756, 776, 791, 806, 820, 839, 853, 862, 866, 867, 865,
                     860, 856, 846, 828, 806, 777, 750, 724, 690, 669, 635, 590,
                     559, 557, 551, 541, 549, 552, 549, 551, 574, 586, 595, 603,
                     609, 606, 592, 588, 576, 580, 565, 557, 570, 582, 587, 576,
                     563, 551, 523, 510]]).T,
                "length": 64,
                "type": "polygon",
                "name": "direct_796-0"},
            ]
        }

    def test_dict_keys(self):
        self.assertTrue(
            "polygons" in self.load_content.keys(), "The 'polygons' entry id missing.")
        for i, pol in enumerate(self.load_content['polygons']):
            self.assertEqual(
                len(pol.keys()), 4, f"Polygon #{i} has wrong number of entries.")
            for k in ["xy", "name", "type", "length"]:
                self.assertTrue(
                    k in pol.keys(), f"Key {k} missing in polygon entry #{i}.")

    def test_load_result(self):
        for k, v in self.load_content.items():
            if k != "polygons":
                self.assertEqual(v, self.correct_content[k],
                                 f"{k}: {v} not equal {self.correct_content[k]}")
            else:
                for i, pol in enumerate(v):
                    for kk, vv in pol.items():
                        if kk in ("length", "type", "name"):
                            self.assertEqual(vv, self.correct_content['polygons'][i][kk])
                        elif kk == "xy":
                            correct = self.correct_content['polygons'][i]['xy']
                            self.assertTrue(
                                np.all(vv == correct),
                                f"\nVertices are not equal, pol #{i=},\n{vv=},"
                                f"\n{correct=}"
                            )


if __name__ == '__main__':
    unittest.main()
